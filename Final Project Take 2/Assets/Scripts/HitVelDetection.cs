﻿using UnityEngine;
using System.Collections;


public class HitVelDetection : MonoBehaviour {
    private LevelManager LevelManager;

	// Use this for initialization
	void Start(){
        LevelManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelManager>();


    }

	void OnCollisionEnter2D(Collision2D collision2D) {

		print (collision2D.relativeVelocity.magnitude);
		if (collision2D.relativeVelocity.magnitude > 15)
        {
            LevelManager.enemiesLeft -= 1;
            if (LevelManager.enemiesLeft == 0)
            {
                LevelManager.checkEnemies();
            }
            Destroy(gameObject);
        }
			
		//Debug.Log ("Check");
	
	}



	

	
	// Update is called once per frame
	void Update () {
		
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    public int enemiesLeft;
	bool killedAllEnemies = false;
    public int levelsLeft;
    public static LevelManager instance = null;

    // Use this for initialization
    void Awake ()
    {
        if (instance == null)
            instance = this;
    }

	void Update ()
    {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("grabbable");
		enemiesLeft = enemies.Length;
		if (Input.GetKeyDown(KeyCode.R))
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);

		if(Input.GetKeyDown(KeyCode.Z))
		{
			enemiesLeft --;
		}

		if(enemiesLeft == 0)
		{
			nextUp();
		}
	}

	void endGame()
	{
		killedAllEnemies = true;
	}

	void OnGUI()
	{
		if(killedAllEnemies)
		{
			GUI.color = Color.black;
			GUI.Label(new Rect (0,0,200,20),"all gone");
		}
		else
		{
			GUI.color = Color.black;
			GUI.Label(new Rect (0,0,200,20),"Enemies Remaining : " + enemiesLeft);
		}
	}

	public void checkEnemies()
	{
		if (enemiesLeft == 0)
		{
			levelsLeft -= 1;
			nextUp();
		}
	}

	void nextUp()
	{
		if (levelsLeft == 2)
			SceneManager.LoadScene ("level2");
		else if (levelsLeft == 1)
			SceneManager.LoadScene ("level3");
		else if (levelsLeft == 0)
			endGame ();

	}
}
